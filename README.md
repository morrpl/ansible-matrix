# Ansible matrix

Installs matrix's synapse server on Debian (10 tested only) based on https://matrix.org/blog/2020/04/06/running-your-own-secure-communication-service-with-matrix-and-jitsi/

Does not handle Lets Encrypt, which can be handled in another ansible role.

